package cmdline;

import org.kohsuke.args4j.Option;

//This class stores the arguments read from the command line.
public class CmdLineArgs {
	
	@Option(required = true, name = "-n", aliases = {"--serverid"}, usage = "Server ID")
	private String serverID;
	
	@Option(required = true, name = "-l", aliases = {"--configfile"}, usage = "Server config file")
	private String serverConfigPath;  //Path to text file containing configuration of servers
	
	public String getServerID() {
		return serverID;
	}

	public String getServerConfigPath() {
		return serverConfigPath;
	}
}