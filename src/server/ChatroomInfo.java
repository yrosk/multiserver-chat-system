package server;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class ChatroomInfo {
	private String chatroomId;

	public ChatroomInfo(String chatroomId) {
		super();
		this.chatroomId = chatroomId;
	}

	public String getChatroomId() {
		return chatroomId;
	}

	public void setChatroomId(String chatroomId) {
		this.chatroomId = chatroomId;
	}
	
	public String toString() {
		return chatroomId;
	}
}
