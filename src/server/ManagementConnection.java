package server;

import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class ManagementConnection extends Connection {

	private static final Logger logger = LoggerFactory.getLogger(ManagementConnection.class);
	
	public ManagementConnection(Socket socket, int clientNum, String name) {
		super(socket, clientNum, name);
	}

	protected void processMessage(JSONObject msg) {
		switch ((String) msg.get("type")) {
		case "lockidentity":
			processLock(msg, IdType.IDENTITY);
			break;
		case "lockroomid":
			processLock(msg, IdType.ROOMID);
			break;
		case "deleteroom":
			processDeleteRoom(msg);
			break;
		default:
			logger.error("Error: invalid message type: " + msg);
			System.exit(1);
		}
	}	
	
	@SuppressWarnings("unchecked")
	private synchronized void processLock(JSONObject msg, IdType idType) {
		String serverid = (String) msg.get("serverid");
		String id = null;
		switch (idType) {
		case IDENTITY:
			id = (String) msg.get("identity");
			break;
		case ROOMID:
			id = (String) msg.get("roomid");
			break;	
		default:
			logger.error("Error: invalid message type: " + msg);
			System.exit(1);
		}

		JSONObject lockMsg = new JSONObject();
		lockMsg.put("serverid", ServerState.getInstance().getId());
		switch (idType) {
		case IDENTITY:
			lockMsg.put("type",  "lockidentity");
			lockMsg.put("identity", id);
			break;
		case ROOMID:
			lockMsg.put("type", "lockroomid");
			lockMsg.put("roomid", id);
			break;
		default:
			logger.error("Error: invalid message type: " + msg);
			System.exit(1);
		}

		if (!ServerState.getInstance().isIdentityInUse(id, idType)) {
			lockMsg.put("locked",  "true");
		} else {
			lockMsg.put("locked",  "false");
		}

		write(lockMsg.toJSONString());

		String response = read();
		JSONObject releaseResponse = null;
		try {
			releaseResponse = (JSONObject) jsonParser.parse(response);
		} catch (ParseException e) {
			logger.error("", e);
			System.exit(1);
		}

		switch (idType) {
		case IDENTITY:
			if (releaseResponse.get("serverid").equals(serverid)) {
				logger.debug("Lock request and release request have same serverid? Yes");
				ServerState.getInstance().removeLock(id, IdType.IDENTITY);
				logger.debug("Released identity lock");
			}  //Otherwise don't release the identity
			break;
		case ROOMID:
			if (releaseResponse.get("approved").equals("true")) {
				// Release the lock and record as new chat room
				ServerState.getInstance().removeLock(id, IdType.ROOMID);
				logger.debug("Released roomid lock");
				RemoteChatroomInfo remoteInfo = new RemoteChatroomInfo(id, serverid);
				ServerState.getInstance().addRemoteChatroom(remoteInfo);
				logger.debug("Added new remote chat room: " + remoteInfo);
			}
			break;
		default:
			logger.error("Error: invalid message type: " + msg);
			System.exit(1);
		}
	}
	
	private void processDeleteRoom(JSONObject msg) {
		String deletedRoomId = (String) msg.get("roomid");
		
		// Remove room from remote chatroom list of this server
		ServerState.getInstance().removeRemoteChatroom(deletedRoomId);
	}
}
