package server;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class ServerInfo {

	private String id;
	private String address;
	private int port;
	private int managementPort;

	public ServerInfo(String id, String address, int port,
			int managementPort) {
		super();
		this.id = id;
		this.address = address;
		this.port = port;
		this.managementPort = managementPort;
	}

	public String getId() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getManagementPort() {
		return managementPort;
	}

	public void setManagementPort(int managementPort) {
		this.managementPort = managementPort;
	}
	
	public String getMainHallId() {
		return "MainHall-" + this.id;
	}

	public String toString() {
		return id + ", " + address + ", " + port + ", " + managementPort;
	}
}