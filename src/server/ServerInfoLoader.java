package server;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class ServerInfoLoader {
	
	private static Logger logger = LoggerFactory.getLogger(ServerInfoLoader.class);

	// Returns a list of all ServerInfo instances created from configuration file
	public static List<ServerInfo> loadServerInfo (String configFilePath) {
		
		List<ServerInfo> serverInfoList = new ArrayList<>();
		//Using try-with-resources so that BufferedReader object is automatically closed
		try (BufferedReader buffReader = new BufferedReader(new FileReader(configFilePath));
		) {
			String configLine = null;
			logger.debug("Loading configuration file...");
			while ((configLine = buffReader.readLine()) != null) {
				//Split the string into substrings delimited by tabs
				String[] configParams = configLine.split("\t");
				//We should have four substrings: server id, server IP,
				//listening port, and management port
				if (configParams.length == 4) {
					String id = configParams[0];
					String address = configParams[1];
					int port = Integer.parseInt(configParams[2]);
					int managementPort = Integer.parseInt(configParams[3]);
					
					//Create ServerInfo object and add it to the list
					ServerInfo serverInfo = new ServerInfo(id, address, port, managementPort);
//					logger.debug("Loaded server: id: " + id
//								+ ", address: " + address
//								+ ", port: " + port
//								+ ", management port: " + managementPort);
					serverInfoList.add(serverInfo);
					logger.debug("Loaded: " + serverInfo);
				}
			}
		} catch (Exception e) {
			logger.error("", e);
			System.exit(1);
		}
	return serverInfoList;
	}
}
