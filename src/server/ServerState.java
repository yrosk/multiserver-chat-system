package server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import server.ServerInfo;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
//Singleton object in charge of server state
public class ServerState {

	private static ServerState instance;

	private ServerInfo serverInfo;

	private List<ServerInfo> availableServers = new CopyOnWriteArrayList<>();
	private Map<String, UserInfo> usersConnected = new ConcurrentHashMap<>();
	private Set<String> lockedIdentities = new HashSet<>();
	private Map<String, LocalChatroomInfo> localChatrooms = new HashMap<>();
	private Map<String, RemoteChatroomInfo> remoteChatrooms = new HashMap<>();
	private Set<String> lockedRoomIds = new HashSet<>();	

	// For storing list of management connections when creating identity or roomid lock.
	// Note: remember to clear this list each time it's used.	
	private List<ManagementConnection> lockMgmtConnections = new ArrayList<>();

	private JSONParser jsonParser = new JSONParser();	

	private static final Logger logger = LoggerFactory.getLogger(ServerState.class);

	private ServerState() {
		super();
	}

	//Must synchronize, otherwise multiple ClientManager objects could get created
	//by different threads
	public static synchronized ServerState getInstance() {
		if (instance == null) {
			instance = new ServerState();
		}
		return instance;
	}

	public synchronized void setServerInfo (ServerInfo info) {
		serverInfo = info;
	}

	public String getId() {
		return serverInfo.getId();
	}

	public int getPort() {
		return serverInfo.getPort();
	}

	public int getManagementPort() {
		return serverInfo.getManagementPort();
	}

	public synchronized List<ServerInfo> getAvailableServers() {
		return availableServers;
	}

	public synchronized void setAvailableServers(List<ServerInfo> list) {
		availableServers = list;
	}

	public synchronized Map<String, UserInfo> getUsersConnected() {
		return usersConnected;
	}

	public synchronized Set<String> getLockedIdentities() {
		return lockedIdentities;
	}

	public Map<String, LocalChatroomInfo> getLocalChatrooms() {
		return localChatrooms;
	}

	public synchronized ArrayList<String>  getAllChatroomNames() {
		ArrayList<String> list = new ArrayList<>();
	
		for (String s : getLocalChatrooms().keySet()) {
			list.add(s);
		}
		for (String s : getRemoteChatrooms().keySet()) {
			list.add(s);
		}
		return list;
	}

	public Map<String, RemoteChatroomInfo> getRemoteChatrooms() {
		return remoteChatrooms;
	}
	
	// Returns ServerInfo of server that manages specified remote chat room
	public synchronized ServerInfo getRemoteChatroomManagerInfo(String roomid) {
		String serverId = remoteChatrooms.get(roomid).getManagingServer();
		for (ServerInfo serverInfo : availableServers) {
			if (serverId.equals(serverInfo.getId())) {
				return serverInfo;
			}
		}
		
		// TODO: throw exception here instead?
		return null;
	}

	public synchronized List<ManagementConnection> getCurrentMgmtConnections() {
		return lockMgmtConnections;
	}

	public synchronized void addUser(UserInfo userInfo) {
		usersConnected.put(userInfo.getIdentity(), userInfo);
		logger.debug("Added user to serverstate: " + userInfo);
		logger.debug("Current users connected: " + usersConnected);
	}

	public synchronized void removeUser(String identity) {
		usersConnected.remove(identity);
		logger.debug("Removed user from server state: " + identity);
		logger.debug("Remaining users connected: " + usersConnected);
	}

	public synchronized void addLocalChatroom(LocalChatroomInfo lcInfo) {
		localChatrooms.put(lcInfo.getChatroomId(), lcInfo);
		logger.debug("Added local chatroom - " + lcInfo);
		
	}
	
	// TODO: check if addLocalChatroom should also inform other servers that room has been added
	@SuppressWarnings("unchecked")
	public synchronized void removeLocalChatroom(LocalChatroomInfo lcInfo) {
		localChatrooms.remove(lcInfo.getChatroomId());
		logger.debug("Removed local chatroom - " + lcInfo);
		
		// Inform other servers of deletion
		JSONObject deleteroomMsg = new JSONObject();
		deleteroomMsg.put("type", "deleteroom");
		deleteroomMsg.put("serverid", getId());
		deleteroomMsg.put("roomid", lcInfo.getChatroomId());
		logger.debug("Informing all other servers of room deletion...");
		int numMgmtConnections = 0;  // TODO: don't really need to keep track of these numbers do we?
		for (ServerInfo serverInfo : getAvailableServers()) {
			try {
				logger.debug("Attempting to connect to server: " + serverInfo);
				Socket mgmtSocket = new Socket(serverInfo.getAddress(), serverInfo.getManagementPort());
				logger.debug("Connection established with server: " + mgmtSocket);
				numMgmtConnections++;

				ManagementConnection mgmtConnection = 
						new ManagementConnection(mgmtSocket, numMgmtConnections, "Room deletion mgmt connection");
				// Write the deletion message
				mgmtConnection.write(deleteroomMsg.toJSONString());
				mgmtConnection.close();
			} catch (IOException e) {
				logger.error("Exception caught when trying to connect to " + serverInfo.getId() + " on port " + serverInfo.getManagementPort(), e);
				System.exit(1);
			}
		}
		logger.debug("Finished informing all other servers of room deletion");
	}

	public synchronized void addRemoteChatroom(RemoteChatroomInfo rcInfo) {
		remoteChatrooms.put(rcInfo.getChatroomId(), rcInfo);
		logger.debug("Added remote chat room - " + rcInfo);
	}
	
	// TODO: check if this and other methods really need to be synchronized
	public synchronized void removeRemoteChatroom(String remoteChatroomId) {
		remoteChatrooms.remove(remoteChatroomId);
		logger.debug("Removed remote chat room - " + remoteChatroomId);
	}
	
	public synchronized void addLockedIdentity(String identity) {
		lockedIdentities.add(identity);
	}
	public synchronized void removeLock(String id, IdType idType) {
		switch (idType) {
		case IDENTITY:
			lockedIdentities.remove(id);
			break;
		case ROOMID:
			lockedRoomIds.remove(id);
		}
	}
	
	//Checks if the identity is not in use by any of the local clients of the server or 
	//is not locked by any other server
	public synchronized boolean isIdentityInUse(String id, IdType idType) {
		switch (idType) {
		case IDENTITY:
			return usersConnected.containsKey(id) ||
				   lockedIdentities.contains(id);
		case ROOMID:
			return localChatrooms.containsKey(id) ||
				   lockedRoomIds.contains(id);
		default:
			logger.error("Invalid id type; exiting program: " + id);
			System.exit(1);
			return false;  // Unreachable; just here to keep compiler quiet
		}
	}	
	
	public synchronized boolean allServersAllowLock(String id, IdType idType) {
		return numServersAllowLock(id, idType) == getAvailableServers().size();
	}
	
	// Requests lock from all other servers and returns true if all allow it.
	public synchronized int numServersAllowLock(String id, IdType idType) {
		// First, make sure we add identity to current server's locked
		// list, so that any other servers asking for a lock will be
		// denied.
		addLockCurrentServer(id, idType);
		
		int numAllowLock = 0;
		int numMgmtConnections = 0;

		logger.debug("Requesting lock with all other servers...");
		for (ServerInfo serverInfo : getAvailableServers()) {
			try {
				logger.debug("Attempting to connect to server: " + serverInfo);
				Socket mgmtSocket = new Socket(serverInfo.getAddress(), serverInfo.getManagementPort());
				logger.debug("Connection established with server: " + mgmtSocket);
				numMgmtConnections++;

				ManagementConnection mgmtConnection = 
						new ManagementConnection(mgmtSocket, numMgmtConnections, "Mgmt connection");
				getCurrentMgmtConnections().add(mgmtConnection);
				logger.debug("Added connection to list");

				if (serverAllowsLock(mgmtConnection, id, idType)) {
					numAllowLock++;
				}
			} catch (IOException e) {
				logger.error("Exception caught when trying to connect to " + serverInfo.getId() + " on port " + serverInfo.getManagementPort(), e);
				System.exit(1);
			}
		}
		logger.debug("Number of servers that allowed lock: " + numAllowLock);	
		return numAllowLock;
	}
	
	@SuppressWarnings("unchecked")
	public synchronized boolean serverAllowsLock(ManagementConnection mgmtConnection, String id, IdType idType) {
		JSONObject lockIdentityMsg = new JSONObject();
		
		lockIdentityMsg.put("serverid",  ServerState.getInstance().getId());
		switch (idType) {
		case IDENTITY:
			lockIdentityMsg.put("type", "lockidentity");
			lockIdentityMsg.put("identity", id);
			break;
		case ROOMID:
			lockIdentityMsg.put("type", "lockroomid");
			lockIdentityMsg.put("roomid", id);
			break;
		default:
			logger.error("Invalid id type; exiting program: " + id);
			System.exit(1);
		}

		mgmtConnection.write(lockIdentityMsg.toJSONString());
		JSONObject lockIdentityResponse = getResponseJSON(mgmtConnection);

		if (lockIdentityResponse.get("locked").equals("true")) {
			logger.debug("Lock allowed: " + mgmtConnection);
			return true;
		} else {
			logger.debug("Lock Denied: " + mgmtConnection);
			return false;
		}
	}
	
	public synchronized void addLockCurrentServer(String id, IdType idType) {
		switch (idType) {
		case IDENTITY:
			lockedIdentities.add(id);
			logger.debug("Added identity lock for current server: " + id);
			break;
		case ROOMID:
			lockedRoomIds.add(id);
			logger.debug("Added roomid lock for current server: " + id);
			break;
		default:
			logger.error("Invalid id type; exiting program: " + id);
			System.exit(1);
		}
	}

	public synchronized void releaseLockAllServers(String id, IdType idType, JSONObject msg) {
		releaseLockOtherServers(id, msg);
		releaseLockCurrentServer(id, idType);
	}	

	public synchronized void releaseLockCurrentServer(String id, IdType idType) {
		switch (idType) {
		case IDENTITY:
			lockedIdentities.remove(id);
			logger.debug("Released identity lock for current server: " + id);
			break;
		case ROOMID:
			lockedRoomIds.remove(id);
			logger.debug("Released roomid lock for current server: " + id);
			break;
		default:
			logger.error("Invalid id type; exiting program: " + id);
			System.exit(1);
		}
	}

	//Send message releasing the lock to all of the other servers:
	public synchronized void releaseLockOtherServers(String id, JSONObject releaseMsg) {
		// Send the release message to each server
		for (ManagementConnection mc : lockMgmtConnections) {
			logger.debug("Releasing identity lock: " + mc);
			mc.write(releaseMsg.toJSONString());
			mc.close();
		}	
		lockMgmtConnections.clear();
		logger.debug("Released lock to all other servers");
	}	
	
	public synchronized void addUserToChatroom(String identity, String chatroom) {
		localChatrooms.get(chatroom).addMember(identity);
	}
	
	public synchronized void removeUserFromChatroom(String identity, String chatroom) {
		localChatrooms.get(chatroom).removeMember(identity);
	}
	
	public synchronized void moveUserLocalChatRoom(UserInfo userInfo, String roomid) {
		removeUserFromChatroom(userInfo.getIdentity(), userInfo.getCurrentChatroom());
		addUserToChatroom(userInfo.getIdentity(), roomid);
		userInfo.setCurrentChatroom(roomid);
	}
	
	public String getMainHallId() {
		return "MainHall-" + getId();
	}
	
	public synchronized void moveUserMainHall(UserInfo userInfo) {
		moveUserLocalChatRoom(userInfo, getMainHallId());
	}
		
	public synchronized void broadcastMessageLocal(String chatroom, JSONObject msg) {
		for (String user : localChatrooms.get(chatroom).getMembers()) {
			UserInfo userInfo = usersConnected.get(user);
			userInfo.getClientConnection().write(msg.toJSONString());
		}
		logger.debug("Broadcast message: " + msg + " to chatroom " + chatroom);
	}
	
	public synchronized void broadcastMessageLocalExcluding(String chatroom, JSONObject msg, String excludeId) {
		for (String user : localChatrooms.get(chatroom).getMembers()) {
			if (!user.equals(excludeId)) {
				UserInfo userInfo = usersConnected.get(user);
				userInfo.getClientConnection().write(msg.toJSONString());
			}
		}
		logger.debug("Broadcast message: " + msg + " to chatroom " + chatroom + " excluding " + excludeId);
	}
	
	public synchronized void createRoom(LocalChatroomInfo lcInfo) {
		localChatrooms.put(lcInfo.getChatroomId(), lcInfo);
		logger.debug(lcInfo + "added to local chatrooms");
	}
	
	public synchronized boolean doesRoomIdExist(String target) {
		return getAllChatroomNames().contains(target);
	}
	
	// TODO: use this method consistently or remove it
	// Returns unmarshalled response from specified connection
	public JSONObject getResponseJSON(Connection connection) {
		String response = connection.read();
		JSONObject responseJSON = null;
		try {
			responseJSON = (JSONObject) jsonParser.parse(response);
		} catch (ParseException e) {
			logger.error("", e);
			System.exit(1);
		}
		return responseJSON;
	}		
}