package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
//An object of this class is instantiated and assigned to each thread
//for listening to client connections and for other server connections
public class Listener implements Runnable {
	
	private int listeningPort;
	private int connectionNum = 0;  // Keep track of number of connections
	
	private final Logger logger = LoggerFactory.getLogger(Listener.class);

	private ListenerType type;

	public Listener(int listeningPort, ListenerType type) {
		super();
		this.listeningPort = listeningPort;
		this.type = type;
	}

	public void run() {

		try (ServerSocket listeningSocket = new ServerSocket(listeningPort)) {

			logger.debug("Server listening on port "  + listeningPort + " for a connection...");	
			
			while (true) {
				//Accept an incoming client connection request
				Socket clientSocket = listeningSocket.accept();
				logger.debug("Client connection accepted: " + clientSocket);
				connectionNum++;
				
				//Create one thread per client connection. Each thread will be responsible for listening
				//for messages from the client and then handing them to the client manager
				//(coordinating singleton) to process them
				logger.debug("Creating new Connection");
				Connection connection = null;
				String threadName = null;
				switch (type) {
					case CLIENT:
						connection = new ClientConnection(clientSocket, connectionNum, "client connection: " + connectionNum);
						threadName = "Client-Cnnectn" + connectionNum;
						break;
					case MANAGEMENT:
						connection = new ManagementConnection(clientSocket, connectionNum, "management connection: "+ connectionNum);
						threadName = "Mgment-Cnnectn" + connectionNum;
						break;
					default:
						logger.error("Invalid Listener type");
						System.exit(1);
				}
				logger.debug("Creating new thread for connection");
				Thread connectionThread = new Thread(connection, threadName);
				connectionThread.start();
			}
		} catch (IOException e) {
			logger.error("Exception caught when trying to listen on port " + listeningPort + " or listening for a connection", e);
			System.exit(1);
		}
	}
}
