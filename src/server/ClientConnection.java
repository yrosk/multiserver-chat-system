package server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class ClientConnection extends Connection {

	UserInfo userInfo = null;

	private static final Logger logger = LoggerFactory.getLogger(ClientConnection.class);

	public ClientConnection(Socket socket, int clientNum, String name) {
		super(socket, clientNum, name);
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	//Validate identity or roomid string. Must be alphanumeric starting with upper or lower case
	//character. Length must be at least 3 and no more than 16 characters long
	private boolean isValidId(String id) {
		return id.matches("[A-Za-z][A-Za-z0-9]{2,15}");
	}	

	protected void processMessage(JSONObject msg) {
		switch ((String) msg.get("type")) {
		case "newidentity":
			newIdentity(msg);
			break;
		case "list":
			commandList();
			break;
		case "who":
			commandWho();
			break;
		case "createroom":
			commandCreateRoom(msg);
			break;
		case "join":
			commandJoinRoom(msg);
			break;
		case "movejoin":
			moveJoinRequest(msg);
			break;
		case "deleteroom":
			commandDeleteRoom(msg);
			break;
		case "message":
			sendMessage(msg);
			break;
		case "quit":
			commandQuit(msg);
			break;
		default:
			logger.error("Error: invalid message type: " + msg);
			System.exit(1);
		}
	}	

	@SuppressWarnings("unchecked")
	private synchronized void newIdentity(JSONObject msg) {
		String identity = (String) msg.get("identity");
		if (!isValidId(identity)) {
			logger.debug("Invalid identity name: must be alphanumeric starting with upper or lower case " +
					"character; length must be at >= 3 && <= 16 characters");
			replyNewIdentityClient(false);
			return;
		}

		// If the identity is in use by any of the local clients, or is locked by any
		// other server (i.e. is in the ServerState's lockedIdentity list), then
		// we skip the locking process and simply send a message saying
		// that the new identity request has been denied.
		if (ServerState.getInstance().isIdentityInUse(identity, IdType.IDENTITY)) {
			replyNewIdentityClient(false);
			return;
		}

		// Check if identity is locked by any other server
		boolean isIdentityApproved = ServerState.getInstance().allServersAllowLock(identity, IdType.IDENTITY);
		replyNewIdentityClient(isIdentityApproved);
		String mainHall = ServerState.getInstance().getMainHallId();
		if (isIdentityApproved) {
			// Also, create UserInfo instance and add to the ServerState's list of connected clients
			setUserInfo(new UserInfo(identity, mainHall, this));
			ServerState.getInstance().addUser(getUserInfo());

			// Place user with this identity into MainHall
			ServerState.getInstance().addUserToChatroom(identity, mainHall);
		}
		// Release lock regardless of whether newidentity was approved
		// Create the release message
		JSONObject releaseMsg = new JSONObject();
		releaseMsg.put("serverid", ServerState.getInstance().getId());
		releaseMsg.put("type", "releaseidentity");
		releaseMsg.put("identity", identity);

		ServerState.getInstance().releaseLockAllServers(identity, IdType.IDENTITY, releaseMsg);

		//TODO: If new identity was approved, broadcast a roomchange message to all the members in the
		// MainHall-s1 room including the connecting client:
		if (isIdentityApproved) {
			JSONObject roomchangeMsg = new JSONObject();
			roomchangeMsg.put("type", "roomchange");
			roomchangeMsg.put("identity", identity);
			roomchangeMsg.put("former", "");
			roomchangeMsg.put("roomid", mainHall);

			// Now broadcast the room change message to all members in the
			// MainHall-sx room, including the connecting client.
			ServerState.getInstance().broadcastMessageLocal(mainHall, roomchangeMsg);
		}
	}

	@SuppressWarnings("unchecked")
	private void replyNewIdentityClient(boolean isIdentityApproved) {
		JSONObject newIdentityMsg = new JSONObject();
		newIdentityMsg.put("type", "newidentity");
		if (isIdentityApproved) {
			newIdentityMsg.put("approved", "true");
		} else {  // Deny creation of new identity
			newIdentityMsg.put("approved", "false");
		}

		write(newIdentityMsg.toJSONString());
	}

	// Server replies with list of all the chat rooms in the system
	@SuppressWarnings("unchecked")
	private void commandList() {
		JSONObject roomlistMsg = new JSONObject();
		JSONArray list = new JSONArray();

		for (String s : ServerState.getInstance().getAllChatroomNames()) {
			list.add(s);
		}
		roomlistMsg.put("type", "roomlist");
		roomlistMsg.put("rooms", list);

		write(roomlistMsg.toJSONString());
	}

	@SuppressWarnings("unchecked")
	private void commandWho() {
		JSONObject whoMsg = new JSONObject();
		JSONArray list = new JSONArray();

		whoMsg.put("type", "roomcontents");
		String roomid = getUserInfo().getCurrentChatroom();
		whoMsg.put("roomid", roomid);
		for (String s : ServerState.getInstance().getLocalChatrooms().get(roomid).getMembers()) {
			list.add(s);
		}
		whoMsg.put("identities", list);
		whoMsg.put("owner", ServerState.getInstance().getLocalChatrooms().get(roomid).getOwner());
		write(whoMsg.toJSONString());
	}

	@SuppressWarnings("unchecked")
	private void commandCreateRoom(JSONObject msg) {
		String id = (String) msg.get("roomid");

		JSONObject createRoomMsg = new JSONObject();
		createRoomMsg.put("type", "createroom");
		createRoomMsg.put("roomid", id);

		if (!isValidId(id)) {
			logger.debug("Invalid roomid: " + id);
			createRoomMsg.put("approved", "false");
			write(createRoomMsg.toJSONString());
			return;
		}

		// Make sure client is not owner of another chat room.
		if (getUserInfo().ownsSomeChatroom()) {
			logger.debug("Client owns chat room");
			createRoomMsg.put("approved", "false");
			write(createRoomMsg.toJSONString());
			return;
		}

		// Send lockroomid message to all the other servers to lock the roomid
		logger.debug("Checking if other servers approve roomid...");
		boolean isRoomIdApproved = ServerState.getInstance().allServersAllowLock(id, IdType.ROOMID);
		String formerRoom = null;
		if (isRoomIdApproved) {
			// First save the current room info to formerRoom
			formerRoom = getUserInfo().getCurrentChatroom();

			// Create the room:
			// First create room info instance
			LocalChatroomInfo newChatroomInfo = new LocalChatroomInfo (id, getUserInfo().getIdentity());
			// Remove user from current chatroom list
			ServerState.getInstance().removeUserFromChatroom(getUserInfo().getIdentity(), getUserInfo().getCurrentChatroom());
			// Update user info to indicate current chatroom
			getUserInfo().setCurrentChatroom(id);
			// Add user to chatInfo member list
			newChatroomInfo.addMember(getUserInfo().getIdentity());
			// Update user info to indicate owns chatroom
			getUserInfo().setChatroomOwned(newChatroomInfo);
			// Add chatroom to server's local chat room list
			ServerState.getInstance().addLocalChatroom(newChatroomInfo);
		}
		// Send different message depending on whether or not room creation was approved
		JSONObject releaseroomidMsg = new JSONObject();
		releaseroomidMsg.put("type", "releaseroomid");
		releaseroomidMsg.put("serverid", ServerState.getInstance().getId());
		if (isRoomIdApproved) {
			releaseroomidMsg.put("approved", "true");
		} else {
			releaseroomidMsg.put("approved", "false");
		}
		ServerState.getInstance().releaseLockAllServers(id, IdType.ROOMID, releaseroomidMsg);
		// TODO: Servers receiving the release room message with 'true' must
		// release the lock and record it as a new chat room

		if (isRoomIdApproved) {
			createRoomMsg.put("approved", "true");
			write(createRoomMsg.toJSONString());

			// Broadcast roomchange message to the client and all the clients that are
			// members of the chat room the user was in
			JSONObject roomchangeMsg = new JSONObject();
			roomchangeMsg.put("type", "roomchange");
			roomchangeMsg.put("identity", getUserInfo().getIdentity());
			roomchangeMsg.put("former", formerRoom);
			roomchangeMsg.put("roomid", id);
			write(roomchangeMsg.toJSONString());
			ServerState.getInstance().broadcastMessageLocal(formerRoom, roomchangeMsg);
		} else {
			createRoomMsg.put("approved", "false");
			write(createRoomMsg.toJSONString());
		}
	}

	@SuppressWarnings("unchecked")
	private void commandJoinRoom(JSONObject msg) {
		// Save current room
		String formerRoomId = getUserInfo().getCurrentChatroom();
		String newRoomId = (String) msg.get("roomid");  // roomid is name of destination room

		// Client can't join another room if owner of current room
		// Client can't join another room if it doesn't exist
		if (    getUserInfo().ownsCurrentChatroom()
				|| !ServerState.getInstance().doesRoomIdExist(newRoomId)) {
			JSONObject nojoinMsg = new JSONObject();
			nojoinMsg.put("type", "roomchange");
			nojoinMsg.put("identity", getUserInfo().getIdentity());
			nojoinMsg.put("former", newRoomId);
			nojoinMsg.put("roomid", newRoomId);

			write(nojoinMsg.toJSONString());
			return;
		}

		// If room is in same server to which client connected, simply place the
		// client in the new room and broadcast roomchange message to the members of the
		// former chat room, the members of the new chat room, and to the client
		// joining the room.

		// Check if room is in same server to which client connected
		if (ServerState.getInstance().getLocalChatrooms().containsKey(newRoomId)) {
			// Place client in new room and set current room
			ServerState.getInstance().moveUserLocalChatRoom(getUserInfo(), newRoomId);

			// Broadcast roomchange message
			JSONObject roomchangeMsg = new JSONObject();
			roomchangeMsg.put("type", "roomchange");
			roomchangeMsg.put("identity",getUserInfo().getIdentity());
			roomchangeMsg.put("former", formerRoomId);
			roomchangeMsg.put("roomid", newRoomId);

			ServerState.getInstance().broadcastMessageLocal(formerRoomId, roomchangeMsg);
			ServerState.getInstance().broadcastMessageLocal(newRoomId, roomchangeMsg);
//			write(roomchangeMsg.toJSONString());  maybe don't need to write because broadcasts to self anyway
		} else {
			// Room is managed by a different server

			// Get address/port of server managing the room
			ServerInfo managingInfo = ServerState.getInstance().getRemoteChatroomManagerInfo(newRoomId);
			String address = managingInfo.getAddress();
			String port = Integer.toString(managingInfo.getPort());

			// First reply to client with a route message redirecting it to another server.
			JSONObject routeMsg = new JSONObject();
			routeMsg.put("type", "route");
			routeMsg.put("roomid", newRoomId);
			routeMsg.put("host", address);
			routeMsg.put("port", port);
			write(routeMsg.toJSONString());

			// Remove client from chatroom member list
			ServerState.getInstance().removeUserFromChatroom(getUserInfo().getIdentity(), formerRoomId);

			// Remove client from current server's list
			ServerState.getInstance().removeUser(getUserInfo().getIdentity());

			// Broadcast roomchange message to all members of the former chat room
			JSONObject roomchangeMsg = new JSONObject();
			roomchangeMsg.put("type", "roomchange");
			roomchangeMsg.put("identity",getUserInfo().getIdentity());
			roomchangeMsg.put("former", formerRoomId);
			roomchangeMsg.put("roomid", newRoomId);

			ServerState.getInstance().broadcastMessageLocal(formerRoomId, roomchangeMsg);
		}
	}

	@SuppressWarnings("unchecked")
	private void moveJoinRequest(JSONObject msg) {
		String roomid = (String) msg.get("roomid");
		String former = (String) msg.get("former");
		String identity = (String) msg.get("identity");

		// If roomid does not exist (i.e. because deleted in middle of route change)
		// then place client in MainHall chat room
		if (!ServerState.getInstance().doesRoomIdExist(roomid)) {
			setUserInfo(new UserInfo(identity, ServerState.getInstance().getMainHallId(), this));
			ServerState.getInstance().addUser(getUserInfo());
			ServerState.getInstance().moveUserMainHall(getUserInfo());
			logger.debug("Room no longer exists, so moving " + identity + " to MainHall");
			roomid = ServerState.getInstance().getMainHallId();
		} else {
			// Roomid does exist

			// Accept the request and add user to client list and place in chat room.
			setUserInfo(new UserInfo(identity, roomid, this));
			ServerState.getInstance().addUser(getUserInfo());
			ServerState.getInstance().addUserToChatroom(identity, roomid);
		}

		// Reply to Maria with serverchange message
		JSONObject serverchangeMsg = new JSONObject();
		serverchangeMsg.put("type", "serverchange");
		serverchangeMsg.put("approved", "true");
		serverchangeMsg.put("serverid", ServerState.getInstance().getId());
		write(serverchangeMsg.toJSONString());	

		// Broadcast roomchange message to all members of room
		JSONObject roomchangeMsg = new JSONObject();
		roomchangeMsg.put("type", "roomchange");
		roomchangeMsg.put("identity", identity);
		roomchangeMsg.put("former", former);
		roomchangeMsg.put("roomid", roomid);
		ServerState.getInstance().broadcastMessageLocal(roomid, roomchangeMsg);
	}

	@SuppressWarnings("unchecked")
	// TODO: should this method (along with others) be put in ServerState class and synchronized?
	// Otherwise other clients could be running through this method at same time
	private void commandDeleteRoom(JSONObject msg) {
		String roomid = (String) msg.get("roomid");

		JSONObject deleteroomMsg = new JSONObject();
		deleteroomMsg.put("type", "deleteroom");
		deleteroomMsg.put("roomid", roomid);
		// Check if client is owner of chat room
		if (   roomid.equals(getUserInfo().getCurrentChatroom()) 
				&& getUserInfo().ownsCurrentChatroom()) {
			// Replies to client who is deleting the room
			deleteroomMsg.put("approved", "true");
			write(deleteroomMsg.toJSONString());

			// Save the list of members of room that will be deleted
			List<String> deletedRoomMembers = new ArrayList<>(ServerState.getInstance().getLocalChatrooms().get(roomid).getMembers());  // TODO: this looks ridiculously long

			JSONObject roomchangeMsg = new JSONObject();
			roomchangeMsg.put("type", "roomchange");
			roomchangeMsg.put("former", roomid);
			roomchangeMsg.put("roomid", ServerState.getInstance().getMainHallId());
			// First broadcast messages before moving, otherwise if move first will get duplicate messages
			for (String memberName : deletedRoomMembers) {
				roomchangeMsg.put("identity", memberName);

				// Send roomchange message about member to each member, including itself
				ServerState.getInstance().broadcastMessageLocal(roomid, roomchangeMsg);

				// Send roomchange message for this member to all clients currently in main hall
				ServerState.getInstance().broadcastMessageLocal(ServerState.getInstance().getMainHallId(), roomchangeMsg);
			}
			// Move each member to main hall (including current)
			for (String memberName : deletedRoomMembers) {
				// Move this member to MainHall
				ServerState.getInstance().moveUserMainHall(ServerState.getInstance().getUsersConnected().get(memberName)); //TODO: clean up these method invocation chains
			}

			// Server removes room and informs other servers of removal
			ServerState.getInstance().removeLocalChatroom(getUserInfo().getChatroomOwned());

			// Update userInfo -- this must be done after moving to mainhall and removing, since these methods depends on the
			// user's UserInfo containing current room
			getUserInfo().setChatroomOwned(null);
		} else {
			// Client is not owner of chat room
			deleteroomMsg.put("approved", "false");
			write(deleteroomMsg.toJSONString());
		}
	}

	@SuppressWarnings("unchecked")
	private void sendMessage(JSONObject msg) {
		String content = (String) msg.get("content");

		JSONObject contentMsg = new JSONObject();
		contentMsg.put("type", "message");
		contentMsg.put("identity", getUserInfo().getIdentity());
		contentMsg.put("content", content);

		ServerState.getInstance().broadcastMessageLocalExcluding(userInfo.getCurrentChatroom(), contentMsg, getUserInfo().getIdentity());
	}

	@SuppressWarnings("unchecked")
	protected void commandQuit(JSONObject msg) {
		String deletedRoomId = getUserInfo().getCurrentChatroom();

		JSONObject clientRoomchangeMsg = new JSONObject();
		clientRoomchangeMsg.put("type", "roomchange");
		clientRoomchangeMsg.put("former", deletedRoomId);
		clientRoomchangeMsg.put("identity", getUserInfo().getIdentity());
		clientRoomchangeMsg.put("roomid", "");

		// Broadcast current user's quit roomchange message to every user in current chatroom, including self
		ServerState.getInstance().broadcastMessageLocal(getUserInfo().getCurrentChatroom(), clientRoomchangeMsg);
			
		// Check if client is owner of a chat room; if so, delete the room according to protocol
		if (getUserInfo().ownsCurrentChatroom()) {
			// Save the list of members of room that will be deleted
			List<String> deletedRoomMembers = new ArrayList<>(ServerState.getInstance().getLocalChatrooms().get(deletedRoomId).getMembers());  // TODO: this looks ridiculously long

			// Broadcast roomchange messages for every other member
			for (String memberId : deletedRoomMembers) {
				if (!memberId.equals(getUserInfo().getIdentity())) {
					JSONObject otherRoomchangeMsg = new JSONObject();
					otherRoomchangeMsg.put("type", "roomchange");
					otherRoomchangeMsg.put("former", deletedRoomId);
					otherRoomchangeMsg.put("identity", memberId);
					otherRoomchangeMsg.put("roomid", ServerState.getInstance().getMainHallId());
					
					// Have each member of deleted room broadcast room change message to every other member, including itself
					ServerState.getInstance().broadcastMessageLocal(getUserInfo().getCurrentChatroom(), otherRoomchangeMsg);
					
					// Also send roomchange message for this member to all clients currently in main hall
					ServerState.getInstance().broadcastMessageLocal(ServerState.getInstance().getMainHallId(), otherRoomchangeMsg);
				}
			}
			
			// Move each member to main hall (excluding current)
			for (String memberName : deletedRoomMembers) {
				if (!memberName.equals(getUserInfo().getIdentity())) {
					// Move this member to MainHall
					ServerState.getInstance().moveUserMainHall(ServerState.getInstance().getUsersConnected().get(memberName)); //TODO: clean up these method invocation chains
				}
			}

			// Server removes room and informs other servers of removal
			ServerState.getInstance().removeLocalChatroom(getUserInfo().getChatroomOwned());

			// Update userInfo -- this must be done after moving to mainhall and removing, since these methods depends on the
			// user's UserInfo containing current room
			getUserInfo().setChatroomOwned(null);
		} else {
			// Client doesn't own room, so just remove from chatroom member list
			ServerState.getInstance().removeUserFromChatroom(getUserInfo().getIdentity(), deletedRoomId);
		}

		// Remove client from client list
		ServerState.getInstance().removeUser(getUserInfo().getIdentity());
	}
	@SuppressWarnings("unchecked")
	protected synchronized void abruptQuit() {
		String deletedRoomId = getUserInfo().getCurrentChatroom();

		String currentUserId = getUserInfo().getIdentity();
		JSONObject clientRoomchangeMsg = new JSONObject();
		clientRoomchangeMsg.put("type", "roomchange");
		clientRoomchangeMsg.put("former", deletedRoomId);
		clientRoomchangeMsg.put("identity", getUserInfo().getIdentity());
		clientRoomchangeMsg.put("roomid", "");

		// Broadcast user's quit roomchange message to every user in previous chatroom, excluding self
		ServerState.getInstance().broadcastMessageLocalExcluding(getUserInfo().getCurrentChatroom(), 
				clientRoomchangeMsg, currentUserId);
			
		// Check if client is owner of a chat room; if so, delete the room according to protocol
		if (getUserInfo().ownsCurrentChatroom()) {
			// Save the list of members of room that will be deleted
			List<String> deletedRoomMembers = new ArrayList<>(ServerState.getInstance().getLocalChatrooms().get(deletedRoomId).getMembers());  // TODO: this looks ridiculously long

			// Broadcast roomchange messages for every other member
			for (String memberId : deletedRoomMembers) {
				if (!memberId.equals(currentUserId)) {
					JSONObject otherRoomchangeMsg = new JSONObject();
					otherRoomchangeMsg.put("type", "roomchange");
					otherRoomchangeMsg.put("former", deletedRoomId);
					otherRoomchangeMsg.put("identity", memberId);
					otherRoomchangeMsg.put("roomid", ServerState.getInstance().getMainHallId());
					
					// Have each member of deleted room broadcast room change message to every other member, including itself,
					// except the client that quit
					ServerState.getInstance().broadcastMessageLocalExcluding(getUserInfo().getCurrentChatroom(), 
							otherRoomchangeMsg, currentUserId);
					
					// Also send roomchange message for this member to all clients currently in main hall
					ServerState.getInstance().broadcastMessageLocal(ServerState.getInstance().getMainHallId(), otherRoomchangeMsg);
				}
			}
			
			// Move each member to main hall (excluding current)
			for (String memberName : deletedRoomMembers) {
				if (!memberName.equals(getUserInfo().getIdentity())) {
					// Move this member to MainHall
					ServerState.getInstance().moveUserMainHall(ServerState.getInstance().getUsersConnected().get(memberName)); //TODO: clean up these method invocation chains
				}
			}

			// Server removes room and informs other servers of removal
			ServerState.getInstance().removeLocalChatroom(getUserInfo().getChatroomOwned());

			// Update userInfo -- this must be done after moving to mainhall and removing, since these methods depends on the
			// user's UserInfo containing current room
			getUserInfo().setChatroomOwned(null);
		} else {
			// Client doesn't own room, so just remove from chatroom member list
			ServerState.getInstance().removeUserFromChatroom(getUserInfo().getIdentity(), deletedRoomId);
		}

		// Remove client from client list
		ServerState.getInstance().removeUser(getUserInfo().getIdentity());
	}
	
	protected synchronized void write(String msg) {
		try {
			writer.write(msg);
			writer.newLine();
			writer.flush();
			logger.debug("Message sent: " + msg + " " + socket);
		} catch (IOException e) {
			logger.error("", e);
			logger.debug("userInfo: " + userInfo);
			logger.debug("calling commandQuit");
			abruptQuit();
		}
	}

}