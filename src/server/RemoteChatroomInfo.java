package server;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class RemoteChatroomInfo extends ChatroomInfo {
	
	private String managingServer;

	public RemoteChatroomInfo(String chatroomId, String managingServer) {
		super(chatroomId);
		this.managingServer = managingServer;
	}

	public String getManagingServer() {
		return managingServer;
	}

	public void setManagingServer(String managingServer) {
		this.managingServer = managingServer;
	}
	
	public String toString() {
		return super.toString() + ", managing server: " + managingServer;
	}
}
