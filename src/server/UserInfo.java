package server;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class UserInfo {
	private String identity;
	private String currentChatroom;
	private ClientConnection clientConnection;
	private LocalChatroomInfo chatroomOwned = null;

	public UserInfo(String identity, String currentChatroom, ClientConnection clientConnection) {
		super();
		this.identity = identity;
		this.currentChatroom = currentChatroom;
		this.clientConnection = clientConnection;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getCurrentChatroom() {
		return currentChatroom;
	}

	public void setCurrentChatroom(String currentChatroom) {
		this.currentChatroom = currentChatroom;
	}

	public ClientConnection getClientConnection() {
		return clientConnection;
	}

	public void setClientConnection(ClientConnection clientConnection) {
		this.clientConnection = clientConnection;
	}

	public LocalChatroomInfo getChatroomOwned() {
		return chatroomOwned;
	}

	public void setChatroomOwned(LocalChatroomInfo chatroomOwned) {
		this.chatroomOwned = chatroomOwned;
	}
	
	public boolean ownsSomeChatroom() {
		return chatroomOwned != null;
	}
	
	public boolean ownsCurrentChatroom() {
		if (chatroomOwned != null) {
			return currentChatroom.equals(chatroomOwned.getChatroomId());
		} else {
			return false;
		}
	}
	
	public String toString() {
		return "identity: " + identity + ", currentChatroom: " + currentChatroom +
				", clientConnecton: " + clientConnection + ", chatroomOwned: " + chatroomOwned;
	}
}