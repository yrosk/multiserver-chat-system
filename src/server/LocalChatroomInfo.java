
package server;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */

public class LocalChatroomInfo extends ChatroomInfo{

	private static final Logger logger = LoggerFactory.getLogger(LocalChatroomInfo.class);

	private String owner;
	private List<String> members = new CopyOnWriteArrayList<>();

	public LocalChatroomInfo(String chatroomId, String owner) {
		super(chatroomId);
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public synchronized List<String> getMembers() {
		return members;
	}

	public synchronized void addMember(String member) {
		members.add(member);
		logger.debug("Added member " + member + " to local chat room " + this);
	}
	
	public synchronized void removeMember(String member) {
		members.remove(member);
		logger.debug("Removed member " + member + " from local chat room " + this);
	}
	
	public String toString() {
		return "id: " + super.getChatroomId() + ", owner: " + owner;
	}
}
