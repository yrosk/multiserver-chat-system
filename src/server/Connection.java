package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
// Represents a connection with a client (Note: this may be an actual chat client, or another server acting as a client
// for management purposes
public abstract class Connection implements Runnable {
	protected Socket socket;
	protected BufferedReader reader;
	protected BufferedWriter writer;
	protected Thread managingThread;
	protected int clientNum;
	protected String name;

	protected JSONParser jsonParser = new JSONParser();

	private static final Logger logger = LoggerFactory.getLogger(Connection.class);

	public Connection(Socket socket, int clientNum, String name) {
		try {
			this.socket = socket;
			this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
			this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
			this.managingThread = Thread.currentThread();
			this.clientNum = clientNum;
			this.name = name;
		} catch (Exception e) {
			logger.error("", e);
			System.exit(1);
		}
	}

	public void run() {
		try {
			String clientMsg = null;
			while ((clientMsg = reader.readLine()) != null) {
				logger.debug("Message received: " + clientMsg);
				JSONObject jsonMsg = null;
				try {
					jsonMsg = (JSONObject) jsonParser.parse(clientMsg);
				} catch (ParseException e) {
					logger.error("", e);
					System.exit(1);
				}
				processMessage(jsonMsg);
			}

			close();
			logger.debug("Finished run() loop. Closing socket and terminating thread.");
		} catch (Exception e) {
			logger.error("", e);
			System.exit(1);
		}
	}	

	protected abstract void processMessage(JSONObject msg);

	protected synchronized String read() {
		try {
			String msg = reader.readLine();
			logger.debug("Message received: " + msg + ' ' + socket);
			return msg;
		} catch (IOException e) {
			logger.error("", e);
			System.exit(1);
			return null;
		}
	}

	protected synchronized void write(String msg) {
		try {
			writer.write(msg);
			writer.newLine();
			writer.flush();
			logger.debug("Message sent: " + msg + " " + socket);
		} catch (IOException e) {
			logger.error("", e);
			System.exit(1);
		}
	}

	protected synchronized void close() {
		//Javadoc says that streams connected to a socket should be closed before the socket
		//itself is closed
		try {
			reader.close();
			writer.close();
			socket.close();
			logger.debug("Closed " + this.toString());
		} catch (IOException e) {
			logger.error("", e);
			System.exit(1);
		}
	}

	public String toString() {
		return name + ", " + socket;
	}
}
