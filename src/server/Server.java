package server;

import java.util.List;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import cmdline.CmdLineArgs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * COMP90015 Distributed Systems
 * Semester 2, 2016
 * Project 1 - Multi-Server Chat System
 * 
 * @author Skye Rajmon McLeman
 */
public class Server {

	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	
	public static void main(String[] args) {
		logger.debug("Current path: " + System.getProperty("user.dir"));

		//Parse command line arguments
		CmdLineArgs argsBean = new CmdLineArgs();
		CmdLineParser cmdLineParser = new CmdLineParser(argsBean);
		try {
			cmdLineParser.parseArgument(args);
			logger.debug("Server ID: " + argsBean.getServerID());
			logger.debug("Server config path: " + argsBean.getServerConfigPath());
		} catch (CmdLineException e) {
			cmdLineParser.printUsage(System.err);
			System.exit(1);
		}

		loadConfiguration(argsBean);
		logger.debug("Available servers:");
		logger.debug(ServerState.getInstance().getAvailableServers().toString());
		
		// Add MainHall chatroom
		LocalChatroomInfo mainHall = new LocalChatroomInfo(ServerState.getInstance().getMainHallId(), "");
		ServerState.getInstance().addLocalChatroom(mainHall);
		
		// Add remote MainHall chatrooms
		addRemoteMainHalls();

		//Create two separate threads using Listener objects: one to listen for chat client
		//connections, and another to listen for server management connections. Each
		//will listen on different ports, as specified in the configuration file.
		Thread clientListenerThread = new Thread(new Listener(ServerState.getInstance().getPort(), ListenerType.CLIENT), 
				"Client-Lstnr");
		Thread managementListenerThread = new Thread(new Listener(ServerState.getInstance().getManagementPort(), ListenerType.MANAGEMENT),
				"Manage-Lstnr");
		
		logger.debug("Starting client and management listener threads...");
		clientListenerThread.start();
		managementListenerThread.start();
	}

	//Load all servers in config file into list of ServerInfo instances
	private static void loadConfiguration(CmdLineArgs argsBean) {
		List<ServerInfo> serverInfos = ServerInfoLoader.loadServerInfo(argsBean.getServerConfigPath());
		ServerInfo currentServerInfo = getCurrentServerInfo(argsBean.getServerID(), serverInfos);
		ServerState.getInstance().setAvailableServers(serverInfos);
		ServerState.getInstance().setServerInfo(currentServerInfo);
	}	
	
	// Set the server info for the ClientManager to appropriate ServerInfo record from list
	// Note: also removes current server info from the list
	private static ServerInfo getCurrentServerInfo (String serverID, List<ServerInfo> infoList) {
		if (infoList != null) {
			for (ServerInfo serverInfo : infoList) {
				if (serverInfo.getId().equals(serverID)) {
					// Make sure we remove current from list, so list will only contains servers other than this one.
					infoList.remove(serverInfo);
					return serverInfo;
				}
			}
		}
		return null;
	}	
	
	// Adds MainHall ids of each other server to list of remote chatrooms
	private static void addRemoteMainHalls() {
		for (ServerInfo serverInfo : ServerState.getInstance().getAvailableServers()) {
			String remoteMainHallId = serverInfo.getMainHallId();
			RemoteChatroomInfo rcInfo = new RemoteChatroomInfo(remoteMainHallId, serverInfo.getId());
			ServerState.getInstance().addRemoteChatroom(rcInfo);
			logger.debug("Added remote mainhall: " + rcInfo);
		}
	}
}